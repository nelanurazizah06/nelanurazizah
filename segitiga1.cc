#include <iostream>
using namespace std;

// Fungsi untuk menghitung kombinasi nCr
int kombinasi(int n, int r) {
    int hasil = 1;
    for (int i = 1; i <= r; i++) {
        hasil *= (n - i + 1);
        hasil /= i;
    }
    return hasil;
}

int main() {
    int N;

    cout << "Masukkan nilai N: ";
    cin >> N;

    for (int n = 0; n < N; n++) {
        for (int i = 0; i < N - n - 1; i++) {
            cout << " ";
        }
        for (int r = 0; r <= n; r++) {
            // Menghitung nilai kombinasi nCr
            int nilai = kombinasi(n, r);
            cout << nilai << " ";
        }
        cout << endl;
    }

    return 0;
}
