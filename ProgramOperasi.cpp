#include <iostream>
using namespace std;

int main() {
    float bilangan1, bilangan2;
    char operasi;

    cout << "Masukkan Bilangan ke 1: ";
    cin >> bilangan1;

    cout << "Masukkan Operator (x, :, +, -): ";
    cin >> operasi;

    cout << "Masukkan Bilangan ke 2: ";
    cin >> bilangan2;

    switch (operasi) {
        case '+':
            cout << "Hasil: " << bilangan1 << " + " << bilangan2 << " = " << bilangan1 + bilangan2 << endl;
            break;
        case '-':
            cout << "Hasil: " << bilangan1 << " - " << bilangan2 << " = " << bilangan1 - bilangan2 << endl;
            break;
        case 'x':
            cout << "Hasil: " << bilangan1 << " x " << bilangan2 << " = " << bilangan1 * bilangan2 << endl;
            break;
        case ':':
            if (bilangan2 != 0) {
                cout << "Hasil: " << bilangan1 << " : " << bilangan2 << " = " << bilangan1 / bilangan2 << endl;
            } else {
                cout << "Error: Pembagian dengan nol tidak valid." << endl;
            }
            break;
        default:
            cout << "Error: Operator yang dimasukkan tidak valid." << endl;
            break;
    }

    return 0;
}
