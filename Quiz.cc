#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    int counter = 1;
    int jumlahData = 0;
    double totalNilai = 0.0;
    double rataRata = 0.0;

    do {
        cout << "Masukkan data ke-" << counter << ": ";
        int data;
        cin >> data;

        if (data != 0) {
            totalNilai += data;
            jumlahData++;
        }

        counter++;
    } while (counter <= 4);

    if (jumlahData > 0) {
        rataRata = totalNilai / jumlahData;
    }

    cout << "Banyaknya Data: " << jumlahData << endl;
    cout << fixed << setprecision(2);
    cout << "Total Nilai Data: " << totalNilai << endl;
    cout << "Rata-rata Nilai Data: " << rataRata << endl;

    return 0;
}
