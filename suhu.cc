#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    // Suhu awal, step, dan suhu akhir
    int suhu_awal = 1;
    int step = 1;
    int suhu_akhir = 30;

    // Mencetak header tabel
    cout<<"============================"<<endl;
    cout << "|Celsius|Fahrenheit|Kelvin |" << endl;
    cout<<"============================"<<endl;

    // Loop untuk menghasilkan baris tabel
    for (int celsius = suhu_awal; celsius <= suhu_akhir; celsius += step) {
        double fahrenheit = (celsius * 9/5) + 32;
        double kelvin = celsius + 273.15;
        cout << "|"<< setw(7)<<celsius << "|" << setw(10) <<fixed<< setprecision(2) << fahrenheit << "|" <<setw(7) << kelvin << "|" << endl;
    }
    
    cout<<"============================"<<endl;

    return 0;
}

