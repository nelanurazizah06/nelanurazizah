#include <iostream>
using namespace std;

int factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * factorial(n-1);
    }
}

int combination(int n, int r) {
    return factorial(n) / (factorial(n-r) * factorial(r));
}

void print_pascal_triangle(int N) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N-i-1; j++) {
            cout << " ";
        }
        for (int j = 0; j <= i; j++) {
            int coef = combination(i, j);
            cout << coef << " ";
        }
        cout << endl;
    }
}

int main() {
    int N;
    cout << "Masukkan N: ";
    cin >> N;
    print_pascal_triangle(N);
    return 0;
}
