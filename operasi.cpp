// Online C++ compiler to run C++ program online
#include <iostream>
using namespace std;
int main() {
// Write C++ code here
int a, b;
cout << "input nilai a : ";
cin >> a;
cout << "input nilai b : ";
cin >> b;
cout << "----------------------------------------" << endl;
cout << " operasi\t|\thasil\toperasi " << endl;
cout << "----------------------------------------" << endl;
cout << a << " + " << b << "\t\t" << "|" << "\t\t" << a + b << endl;
cout << a << " - " << b << "\t\t" << "|" << "\t\t" << a - b << endl;
cout << a << " * " << b << "\t\t" << "|" << "\t\t" << a * b << endl;
cout << a << " div " << b << "\t\t" << "|" << "\t\t" << a / b << endl;
cout << a << " mod " << b << "\t\t" << "|" << "\t\t" << a % b << endl;
cout << "-----------------------------------------" << endl;
return 0;
}
